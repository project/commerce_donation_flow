<?php

namespace Drupal\Tests\commerce_donation_flow\Functional;

use Behat\Mink\Element\NodeElement;
use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_payment\Entity\PaymentGateway;
use Drupal\Tests\commerce\Functional\CommerceBrowserTestBase;
use Drupal\user\Entity\Role;

/**
 * Tests the checkout of an order.
 *
 * @group commerce_donation_flow
 */
class DonationOrderTest extends CommerceBrowserTestBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;


  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_product',
    'commerce_order',
    'commerce_cart',
    'commerce_checkout',
    'commerce_payment',
    'commerce_donation_flow',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function getAdministratorPermissions() {
    return array_merge(
      [
        'make donation',
      ],
      parent::getAdministratorPermissions()
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->store->set('billing_countries', ['US']);
    $this->store->save();

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = PaymentGateway::create([
      'id' => 'manual',
      'label' => 'Manual',
      'plugin' => 'manual',
      'configuration' => [
        'display_label' => 'Cash on delivery',
        'instructions' => [
          'value' => 'Sample payment instructions.',
          'format' => 'plain_text',
        ],
      ],
    ]);
    $payment_gateway->save();

    /** @var \Drupal\commerce_order\Entity\OrderType $defaultOrders */
    $defaultOrders = $this->container->get('entity_type.manager')->getStorage('commerce_order_type')->load('default');
    // Use our donation flow.
    $defaultOrders->setThirdPartySetting('commerce_checkout', 'checkout_flow', 'donation_flow');
    $defaultOrders->save();

    $this->placeBlock('commerce_donation_flow_link');
    $this->placeBlock('commerce_checkout_progress');
  }

  /**
   * Tests anonymous and authenticated checkout.
   */
  public function testDonations() {
    $this->drupalLogout();
    $this->grantPermissions(Role::load(Role::ANONYMOUS_ID), ['make donation']);
    $donate_link = $this->getSession()->getPage()->findLink('Donate');
    $donate_link->click();
    $this->assertCheckoutProgressStep('Your Donation');
    $this->submitForm([], 'Continue to Payment');
    $this->assertCheckoutProgressStep('Billing Information');
    $this->assertSession()->pageTextContainsOnce('Donating $100');
    $this->submitForm(
      [
        'contact_information[email]' => 'guest@example.com',
        'contact_information[email_confirm]' => 'guest@example.com',
        'payment_information[billing_information][address][0][address][given_name]' => 'John',
        'payment_information[billing_information][address][0][address][family_name]' => 'Smith',
        'payment_information[billing_information][address][0][address][organization]' => 'Centarro',
        'payment_information[billing_information][address][0][address][address_line1]' => '9 Drupal Ave',
        'payment_information[billing_information][address][0][address][postal_code]' => '94043',
        'payment_information[billing_information][address][0][address][locality]' => 'Mountain View',
        'payment_information[billing_information][address][0][address][administrative_area]' => 'CA',
      ],
      'Donate $100'
    );
    $this->assertCheckoutProgressStep('Complete');
    $this->assertSession()->pageTextContains('thank you for donating');
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = Order::load(1);
    $this->assertFalse($order->getTotalPrice()->isZero());

    // Test second order.
    $this->drupalLogin($this->adminUser);
    $donate_link = $this->getSession()->getPage()->findLink('Donate');
    $donate_link->click();
    $this->assertCheckoutProgressStep('Your Donation');
    $this->submitForm([], 'Continue to Payment');
    $this->assertCheckoutProgressStep('Billing Information');
    $this->assertSession()->pageTextContainsOnce('Donating $100');
    $this->submitForm(
      [
        'payment_information[billing_information][address][0][address][given_name]' => 'John',
        'payment_information[billing_information][address][0][address][family_name]' => 'Smith',
        'payment_information[billing_information][address][0][address][organization]' => 'Centarro',
        'payment_information[billing_information][address][0][address][address_line1]' => '9 Drupal Ave',
        'payment_information[billing_information][address][0][address][postal_code]' => '94043',
        'payment_information[billing_information][address][0][address][locality]' => 'Mountain View',
        'payment_information[billing_information][address][0][address][administrative_area]' => 'CA',
      ],
      'Donate $100'
    );
    $this->assertCheckoutProgressStep('Complete');
    $this->assertSession()->pageTextContains('thank you for donating');
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = Order::load(2);
    $this->assertFalse($order->getTotalPrice()->isZero());
  }

  /**
   * Asserts the current step in the checkout progress block.
   *
   * @param string $expected
   *   The expected value.
   */
  protected function assertCheckoutProgressStep($expected) {
    $current_step_node = $this->getSession()->getPage()->find(
      'css',
      '.checkout-progress--step__current'
    );
    if (!$current_step_node instanceof NodeElement) {
      $this->fail('Checkout progress element is not present.');
    }
    $current_step = $current_step_node->getText();
    $this->assertEquals($expected, $current_step);
  }

}
