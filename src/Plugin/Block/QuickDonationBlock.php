<?php

namespace Drupal\commerce_donation_flow\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a donation block.
 *
 * @Block(
 *   id = "commerce_donation_flow_quick",
 *   admin_label = @Translation("Quick Donation"),
 *   category = @Translation("Commerce Donation Flow")
 * )
 */
class QuickDonationBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * {@inheritdoc}
   */
  final public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    // @see https://phpstan.org/blog/solving-phpstan-error-unsafe-usage-of-new-static
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->formBuilder = $container->get('form_builder');
    return $instance;

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm(
    $form,
    FormStateInterface $form_state
  ) {
    $form = parent::blockForm($form, $form_state);
    $levels = $this->configuration['levels'] ?? [];
    $form['levels'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Preset donation levels'),
      '#description' => $this->t(
        'These are fixed amounts offered for choice to a user.'
      ),
      '#tree' => TRUE,
    ];
    for ($index = 0; $index < 4; $index++) {
      $form['levels'][$index] = [
        '#type' => 'number',
        '#default_value' => $levels[$index] ?? 5,
        '#min' => 5,
        '#step' => 5,
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['levels'] = $form_state->getValue('levels');
  }

  /**
   * Builds the donation form.
   *
   * @return array
   *   A render array.
   */
  public function build() {
    $levels = $this->configuration['levels'];
    $links = [];
    foreach ($levels as $delta => $amount) {
      $links[$delta] = [
        '#type' => 'link',
        '#title' => '$' . $amount,
        '#url' => Url::fromRoute(
          'commerce_donation_flow.donation.quick',
          ['amount' => $amount]
        ),
      ];
    }
    $build = [
      '#theme' => 'block__commerce_donation_flow_quick',
      'links' => $links,
    ];
    return $build;
  }

}
