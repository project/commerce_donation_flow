<?php

namespace Drupal\commerce_donation_flow\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Url;
use Drupal\path_alias\AliasManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'DonateBlock' block.
 *
 * @Block(
 *  id = "commerce_donation_flow_link",
 *  admin_label = @Translation("Donate"),
 *  category = @Translation("Commerce Donation Flow")
 * )
 */
class DonateBlock extends BlockBase implements ContainerFactoryPluginInterface {

  // Modeled on \Drupal\user\Plugin\Block\UserLoginBlock.
  use RedirectDestinationTrait;

  /**
   * Injected service.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * Injected service.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * {@inheritdoc}
   */
  final public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    // @see https://phpstan.org/blog/solving-phpstan-error-unsafe-usage-of-new-static
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->pathMatcher = $container->get('path.matcher');
    if ($container->has('path_alias.manager')) {
      $instance->aliasManager = $container->get('path_alias.manager');
    }
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    $config = parent::defaultConfiguration();
    $config['return_path'] = FALSE;
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $form['return_path'] = [
      '#type' => 'checkbox',
      '#title' => $this
        ->t('Add a return path to urls.'),
      '#default_value' => $this->configuration['return_path'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['return_path'] = $form_state->getValue('return_path') ? TRUE : FALSE;
    parent::blockSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    /** @var \Drupal\Core\StringTranslation\TranslatableMarkup[] $giftTypes */
    $options = [];
    if ($this->configuration['return_path']) {
      $returnPath = ['donate_return' => $this->determineReturnPath()];
      $options = ['query' => $returnPath];
    }
    // Set #theme to links__donate to get an automatic template suggestion.
    $build = [
      '#theme' => 'links__donate',
      '#cache' => ['contexts' => ['url.path', 'url.query_args']],
      '#links' => [
        'default' => [
          'title' => $this->t('Donate'),
          'url' => Url::fromRoute('commerce_donation_flow.donation.default', [], $options),
        ],
      ],
    ];
    return $build;
  }

  /**
   * Helper function to create a user friendly return path.
   *
   * @return string
   *   The return path.
   */
  protected function determineReturnPath() {
    if ($this->pathMatcher->isFrontPage()) {
      return '/';
    }
    $returnPath = $this->getRedirectDestination()->get();
    if ($this->aliasManager instanceof AliasManagerInterface) {
      return $this->aliasManager->getAliasByPath($returnPath);
    }
    return $returnPath;
  }

}
