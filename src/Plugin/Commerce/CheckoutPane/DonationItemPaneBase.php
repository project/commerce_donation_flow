<?php

namespace Drupal\commerce_donation_flow\Plugin\Commerce\CheckoutPane;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\EventSubscriber\AjaxResponseSubscriber;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\field_group\FormatterHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DonationItemPaneBase holds common donation pane methods.
 *
 * @package Drupal\commerce_donation_flow\Plugin\Commerce\CheckoutPane
 */
class DonationItemPaneBase extends CheckoutPaneBase {

  /**
   * The OrderItem that models the donation.
   *
   * @var \Drupal\commerce_order\Entity\OrderItem
   */
  protected $donationItem;

  /**
   * Injected service: Used to detect if Field Group module is installed.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * FormDisplay object for this pane.
   *
   * @var \Drupal\Core\Entity\Display\EntityFormDisplayInterface
   */
  protected $formDisplay;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $checkout_flow);
    // The order should now be populated.
    if (!empty($instance->order)) {
      // Insure an order item is set and easily found.
      $items = $instance->order->getItems();
      if (empty($items)) {
        /* @see \Drupal\commerce_donation_flow\NewOrder::get() */
        throw new \UnexpectedValueException('Order Items not properly initialized');
      }
      // Only one donation OrderItem supported.
      $donations = array_filter($items, function ($item) {
        /** @var \Drupal\commerce_order\Entity\OrderItemInterface $item */
        return $item->bundle() == 'donation';
      });
      $instance->donationItem = reset($donations);
    }
    $instance->moduleHandler = $container->get('module_handler');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    if (!$this->donationItem instanceof OrderItemInterface) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Setter used in create() methods of extending classes.
   *
   * @param string $displayMode
   *   The display mode id.
   */
  protected function setFormDisplay($displayMode) {
    if ($this->donationItem instanceof OrderItemInterface) {
      $this->formDisplay = EntityFormDisplay::collectRenderDisplay(
        $this->donationItem,
        $displayMode
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form['#title_display'] = 'invisible';
    // Check for ajax build.
    $input = $form_state->getUserInput();
    if (isset($input[AjaxResponseSubscriber::AJAX_REQUEST_PARAMETER])) {
      // Render any status messages:
      $pane_form['messages'] = [
        '#type' => 'status_messages',
        '#weight' => -90,
      ];
    }
    // Adapted from ParagraphsWidget.php:670:
    // @@see https://www.drupal.org/node/2640056 Remove when fixed.
    if ($this->moduleHandler->moduleExists('field_group')) {
      $context = [
        'entity_type' => $this->donationItem->getEntityTypeId(),
        'bundle' => $this->donationItem->bundle(),
        'entity' => $this->donationItem,
        'context' => 'form',
        'display_context' => 'form',
        'mode' => $this->formDisplay->getMode(),
      ];
      field_group_attach_groups($pane_form, $context);
      $pane_form['#pre_render'][] = [FormatterHelper::class, 'formProcess'];
    }
    $this->formDisplay->buildForm($this->donationItem, $pane_form, $form_state);
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function validatePaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    // Our form comes from the entity form builder.
    $form_state->cleanValues();
    $this->buildDonationItem($pane_form, $form_state);
    $this->formDisplay->validateFormValues($this->donationItem, $pane_form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitPaneForm(array &$pane_form, FormStateInterface $form_state, array &$complete_form) {
    $this->donationItem->save();
  }

  /**
   * Builds an updated OrderItem object based upon the submitted form values.
   *
   * Based on \Drupal\inline_entity_form\Form\EntityInlineForm::buildEntity:
   *
   * @param array $paneForm
   *   The entity form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  protected function buildDonationItem(array $paneForm, FormStateInterface $form_state) {
    $this->formDisplay->extractFormValues($this->donationItem, $paneForm, $form_state);
    if ($this->donationItem->hasField('field_donation_amount') && !$this->donationItem->field_donation_amount->isEmpty()) {
      $price = $this->donationItem->field_donation_amount->first()->toPrice();
      $this->donationItem->setUnitPrice($price, TRUE);
    }
    $donationTitle = $this->donationItem->getTitle();
    if ($this->donationItem->hasField('field_gift_type')) {
      $donationTitle .= ' - ';
      $donationTitle .= $this->donationItem->field_gift_type->value;
      $recurringValue = NULL;
      if ($this->donationItem->hasField('field_recurring_begins') && $this->donationItem->field_gift_type->value == 'recurring') {
        $timezone = date_default_timezone_get();
        $setTime = new DrupalDateTime('+1 month', $timezone);
        // Adjust the date for storage.
        $setTime->setTimezone(new \DateTimezone(DateTimeItemInterface::STORAGE_TIMEZONE));
        $recurringValue = $setTime->format(DateTimeItemInterface::DATE_STORAGE_FORMAT);
        $this->donationItem->field_recurring_begins->setValue(['value' => $recurringValue]);
      }
    }
    $this->donationItem->setTitle($donationTitle);
    // Invoke all specified builders for copying form values to entity fields.
    if (isset($paneForm['#entity_builders'])) {
      foreach ($paneForm['#entity_builders'] as $function) {
        call_user_func_array(
          $function,
          [
            $this->donationItem->getEntityTypeId(),
            $this->donationItem,
            &$paneForm,
            &$form_state,
          ]
        );
      }
    }
  }

  /**
   * Helper method to build the pane summary given a view mode.
   *
   * @param string $viewMode
   *   The view mode to build.
   *
   * @return array
   *   The render array.
   */
  protected function doSummaryBuild($viewMode) {
    $viewBuilder = $this->entityTypeManager->getViewBuilder($this->donationItem->getEntityTypeId());
    $entityView = $viewBuilder->view($this->donationItem, $viewMode, $this->donationItem->language()
      ->getId());
    $renderArray = $viewBuilder->build($entityView);
    $build = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'donation-summary-pane',
          'donation-pane',
        ],
      ],
      'summary' => [
        '#theme' => 'commerce_donation_flow_summary',
        '#content' => $renderArray,
      ],
    ];
    return $build;
  }

}
