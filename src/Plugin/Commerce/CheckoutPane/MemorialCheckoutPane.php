<?php

namespace Drupal\commerce_donation_flow\Plugin\Commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutFlow\CheckoutFlowInterface;
use Drupal\commerce_order\Entity\OrderItemInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * A custom pane for creating Donation OrderItems.
 *
 * Form states and additional displays are attached using field groups
 * set on the Order Item 'memorial' display and processed in
 * maw_luminate_field_group_pre_render_alter()
 *
 * @CommerceCheckoutPane(
 *  id = "commerce_memorial_pane",
 *  label = @Translation("Dedication"),
 *  display_label = @Translation("Honoree Information"),
 *  default_step = "dedication",
 *  wrapper_element = "fieldset",
 * )
 */
class MemorialCheckoutPane extends DonationItemPaneBase {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition, CheckoutFlowInterface $checkout_flow = NULL) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition, $checkout_flow);
    $instance->setFormDisplay('memorial');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function isVisible() {
    if (
      $this->donationItem instanceof OrderItemInterface
      && $this->donationItem->hasField('field_designated')
    ) {
      return $this->donationItem->field_designated->first()->value == 1;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form = parent::buildPaneForm($pane_form, $form_state, $complete_form);
    return $pane_form;
  }

  /**
   * {@inheritdoc}
   */
  public function buildPaneSummary() {
    $viewMode = 'memorial';
    return $this->doSummaryBuild($viewMode);
  }

}
