<?php

namespace Drupal\commerce_donation_flow;

use Drupal\commerce_order\Entity\Order;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_order\Entity\OrderItem;
use Drupal\commerce_price\Price;
use Drupal\commerce_store\CurrentStoreInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Service for creating a new order with a donation item.
 *
 * Used in the checkout flow and the donateNow route method
 * in DonationController.
 */
class NewOrder {

  /**
   * The current store.
   *
   * @var \Drupal\commerce_store\CurrentStoreInterface
   */
  protected $currentStore;

  /**
   * Injected service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $user;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $currentRequest;

  /**
   * The donation bundle configuration..
   *
   * @var \Drupal\commerce_order\Entity\OrderItemTypeInterface
   */
  protected $donationEntityConfig;

  /**
   * NewOrder constructor.
   *
   * @param \Drupal\commerce_store\CurrentStoreInterface $currentStore
   *   Injected service.
   * @param \Drupal\Core\Session\AccountProxyInterface $accountProxy
   *   Injected service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Injected service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Injected service.
   */
  public function __construct(CurrentStoreInterface $currentStore, AccountProxyInterface $accountProxy, RequestStack $requestStack, EntityTypeManagerInterface $entityTypeManager) {
    $this->currentStore = $currentStore;
    $this->user = $accountProxy;
    $this->currentRequest = $requestStack->getCurrentRequest();
    $this->donationEntityConfig = $entityTypeManager->getStorage('commerce_order_item_type')->load('donation');
  }

  /**
   * Creates a new Order object for the donation.
   *
   * @param string $giftType
   *   Optional gift type.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   The new order object.
   *
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public function get($giftType = 'single') {
    /** @var \Drupal\commerce_order\Entity\Order $order */
    $order = Order::create(
      [
        'type' => $this->donationEntityConfig->get('orderType') ?? 'default',
        'state' => 'draft',
        'store_id' => $this->currentStore->getStore()->id(),
        'uid' => $this->user->id(),
        'cart' => TRUE,
      ]
    );

    // Starts at 0.
    $order = $this->addDonationItem($order, 0, $giftType);

    return $order;
  }

  /**
   * Adds a donation order item to the given order.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The order object to which the item is being added.
   *
   * @param int $price
   *   Optional initial price of the donation order item.
   *
   * @param string $giftType
   *   Optional gift type.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   An order with donation item added.
   *
   * @throws \Drupal\Core\TypedData\Exception\ReadOnlyException
   */
  public function addDonationItem(OrderInterface $order, int $price = 0, string $giftType = 'single') {
    $price = new Price(
      $price,
      $this->currentStore->getStore()->getDefaultCurrencyCode()
    );
    $donation = OrderItem::create(
      [
        'type' => 'donation',
        'title' => 'Donation',
      ]
    );
    /** @var \Drupal\commerce_order\Entity\OrderItem $donation */
    $donation->setUnitPrice($price);
    $donation->field_gift_type->setValue($giftType);
    $this->prepopulate($donation);
    $order->addItem($donation);

    return $order;
  }

  /**
   * Prepopulate any matching OrderItem fields with query paramters.
   *
   * @param \Drupal\commerce_order\Entity\OrderItem $donation
   *   Our OrderItems are donations.
   */
  public function prepopulate(OrderItem $donation) {
    foreach ($this->currentRequest->query as $key => $value) {
      if ($donation->hasField('field_' . $key)) {
        $donation->set('field_' . $key, $value);
      }
    }
  }

}
